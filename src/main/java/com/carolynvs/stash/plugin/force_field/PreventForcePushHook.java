package com.carolynvs.stash.plugin.force_field;

import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.hook.repository.*;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.StandardRefType;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.util.MoreCollectors;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//@Slf4j
public class PreventForcePushHook implements PreRepositoryHook<RepositoryPushHookRequest>
{
    private static final Logger log = LoggerFactory.getLogger(ForceFieldConfigServlet.class);


    private final CommitService commitService;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final I18nService i18nService;

    final private PathMatcher pathMater = new AntPathMatcher();


    @Autowired
    public PreventForcePushHook(@ComponentImport CommitService commitService,
                                @ComponentImport I18nService i18nService,
                                @ComponentImport PluginSettingsFactory pluginSettingsFactory )
    {
        this.commitService = commitService;
        this.i18nService = i18nService;
        this.pluginSettingsFactory = pluginSettingsFactory;

    }


    @Nonnull
    @Override
    public RepositoryHookResult preUpdate(@Nonnull PreRepositoryHookContext context, @Nonnull RepositoryPushHookRequest request) {

        final List<String> protectedRefs = listProjectedRefs(context);

        if(protectedRefs == null && protectedRefs.isEmpty())
            return  RepositoryHookResult.accepted();

        List<RefChange> updates = request.getRefChanges()
                .stream()
                .filter(refChange -> refChange.getType() == RefChangeType.UPDATE
                                  || refChange.getType() == RefChangeType.DELETE)
//                .map(x->{
//                    log.warn("----<<<<---- {} {} {}", x.getType(), x.getRef().getDisplayId(), x.getRef().getType());
//                    return x;})
                .filter(refChange -> isProtectedRef(protectedRefs, refChange))
                .collect(MoreCollectors.toImmutableList());

        return updates.isEmpty() ? RepositoryHookResult.accepted() :
                updates.stream()
                        .filter(refChange -> refChange.getRef().getType() == StandardRefType.TAG)
                        .findFirst()
                        .map(this::rejectTagUpdate)
                        .orElseGet(() -> {
                            context.registerCommitCallback(new PreventForcePushHook.ForcePushCommitCallback(),
                                    RepositoryHookCommitFilter.REMOVED_FROM_ANY_REF);
                            return RepositoryHookResult.accepted();
                        });
    }


    private RepositoryHookResult rejectRemovedCommit(CommitRemovedDetails details) {
        return RepositoryHookResult.rejected(
                this.i18nService.getMessage("force-field.hook.rejected"),
                this.i18nService.getMessage("force-field.hook.rejected.details.commit",
                        details.getCommit().getId(), details.getRef().getDisplayId()));
    }

    private RepositoryHookResult rejectTagUpdate(RefChange refChange) {
        return RepositoryHookResult.rejected(
                this.i18nService.getMessage("force-field.hook.rejected"),
                this.i18nService.getMessage("force-field.hook.rejected.details.tag",
                        refChange.getRef().getDisplayId()));
    }

    private class ForcePushCommitCallback implements PreRepositoryHookCommitCallback {
        private CommitRemovedDetails details;


        @Nonnull
        public RepositoryHookResult getResult() {
            return this.details == null ? RepositoryHookResult.accepted() : rejectRemovedCommit(this.details);
        }

        public boolean onCommitRemoved(@Nonnull CommitRemovedDetails details) {
            this.details = details;
            return false;
        }

    }

    List<String> listProjectedRefs(@Nonnull PreRepositoryHookContext context) {
        List<String> protectedRefs = getProtectedRefs(context);
        log.info("protected refs: {}", protectedRefs);

        if (protectedRefs == null || protectedRefs.isEmpty()) {
            PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
            Map<String, String> settingsMap =
                    (Map<String, String>) pluginSettings.get(ForceFieldConfigServlet.SETTINGS_MAP);

            protectedRefs = getReferencies(settingsMap);
            log.info("Global protected refs: {}", protectedRefs);
        }
        return protectedRefs;
    }

    <V> List<String> getReferencies(Map<String, V> settingsMap) {
        ArrayList<String> protectedRefs = new ArrayList<String>();
        Object obj = settingsMap.get(ForceFieldConfigServlet.SETTINGS_REFS);
        if (obj != null && obj instanceof String) {
            String references = (String)obj;
            if (!references.isEmpty()) {
                String[] refs = references.split(" ");
                for (String ref : refs) {
                    ref = ReferenceCleaner.fixUnmatchableRegex(ref);
                    protectedRefs.add(ref);
                }
            }
        }
        return protectedRefs;
    }

    List<String> getProtectedRefs(@Nonnull Settings settings)
    {
        return getReferencies(settings.asMap());
    }
    List<String> getProtectedRefs(@Nonnull RepositoryHookContext context) {
        return getProtectedRefs(context.getSettings());
    }



    private boolean isProtectedRef(List<String> restrictedRefs, RefChange refChange)
    {
        String refId = refChange.getRef().getId();
        for(String refPattern : restrictedRefs)
        {
            if(pathMater.match(refPattern, refId))
            {
                return true;
            }
        }

        return false;
    }

}
