package com.carolynvs.stash.plugin.force_field;

import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookService;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.i18n.I18nService;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;

/**
 * @author Jim Bethancourt
 */
public class PreventForcePushHookTest {

    @Mock private RepositoryHookContext repositoryHookContext;
    @Mock private Settings settings;
    @Mock private CommitService commitService;
    @Mock private I18nService i18nService;
    @Mock private PluginSettingsFactory pluginSettingsFactory;

    PreventForcePushHook hook;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(repositoryHookContext.getSettings()).thenReturn(settings);
        hook = new PreventForcePushHook(commitService, i18nService, pluginSettingsFactory);
    }

    @Test
    public void testGetProtectedRefsNoRef() {
        when(settings.asMap()).thenReturn(new HashMap<>());
        List<String> refs = hook.getProtectedRefs(repositoryHookContext);
        assertThat(refs).isEmpty();
    }

    @Test
    public void testGetProtectedRefsNullRef() {
        Map<String, Object> refMap = new HashMap<>();
        refMap.put("references", null);
        when(settings.asMap()).thenReturn(refMap);
        when(settings.getString("references")).thenReturn(null);
        List<String> refs = hook.getProtectedRefs(repositoryHookContext);
        assertThat(refs).isEmpty();
    }

    @Test
    public void testGetProtectedRefsEmptyRef() {
        Map<String, Object> refMap = new HashMap<>();
        refMap.put("references", "");
        when(settings.asMap()).thenReturn(refMap);
        when(settings.getString("references")).thenReturn("");
        List<String> refs = hook.getProtectedRefs(repositoryHookContext);
        assertThat(refs).isEmpty();
    }

    @Test
    public void testGetProtectedRefsHasSingleRef() {
        Map<String, Object> refMap = new HashMap<>();
        refMap.put("references", "master");
        when(settings.asMap()).thenReturn(refMap);
        when(settings.getString("references")).thenReturn("master");
        List<String> refs = hook.getProtectedRefs(repositoryHookContext);
        assertThat(refs).contains("**/master");
    }

    @Test
    public void testGetProtectedRefsHasMultipleRefs() {
        Map<String, Object> refMap = new HashMap<>();
        refMap.put("references", "origin master");
        when(settings.asMap()).thenReturn(refMap);
        when(settings.getString("references")).thenReturn("origin master");
        List<String> refs = hook.getProtectedRefs(repositoryHookContext);
        assertThat(refs).contains("**/origin");
        assertThat(refs).contains("**/master");
    }
}
