package com.carolynvs.stash.plugin.force_field;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;


@Slf4j
public class TestPatterns {

    @Test
    //checking spring ant patterns
    public void testTagMatches() {
        PathMatcher matcher = new AntPathMatcher();

        Assert.assertTrue(matcher.match("*", "v2.2.-"));
        Assert.assertTrue( matcher.match("v{:[0-9].*}", "v2.0"));
        Assert.assertTrue( matcher.match("v{:[0-9][0-9.]*(-[RH][0-9]+)*}", "v2.0.0"));
        Assert.assertTrue( matcher.match("v{:[0-9][0-9.]*(-[RH][0-9]+)*}", "v2.0.0-R0001"));
        Assert.assertFalse(matcher.match("v{:[0-9].*}", "vers2.0"));
        Assert.assertTrue(matcher.match("jenkins{:-[0-9].*}", "jenkins-2.0"));
    }
}
